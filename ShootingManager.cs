using System.Collections.Generic;
using UnityEngine;

public class ShootingManager : MonoBehaviour
{
    public List<ObjectController> objectsToShoot = new List<ObjectController>();

    void OnMouseDown()
    {
        // Check for user input to shoot or return objects
        foreach (ObjectController obj in objectsToShoot)
        {
            if (obj != null)
            {
                obj.ShootOrReturnObject();
            }
            else
            {
                Debug.LogWarning("ObjectController is null in objectsToShoot list.");
            }
        }
    }
}