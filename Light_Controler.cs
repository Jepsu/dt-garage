using UnityEngine;

public class SwitchControl : MonoBehaviour
{
    public Light[] areaLights; // Array of Area Light components
    private bool isLightOn = false;

    private void Start()
    {
        // Ensure all lights are initially turned off
        foreach (Light areaLight in areaLights)
        {
            areaLight.enabled = false;
        }
    }

    private void Update()
    {
        // Check for a mouse click
        if (Input.GetMouseButtonDown(0)) // 0 represents the left mouse button
        {
            // Cast a ray from the mouse position into the scene
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            // Check if the ray hits the switch object
            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject == this.gameObject)
            {
                isLightOn = !isLightOn; // Toggle the light state

                // Activate or deactivate all Area Lights based on the switch state
                foreach (Light areaLight in areaLights)
                {
                    areaLight.enabled = isLightOn;
                }
            }
        }
    }
}
