using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneRestart : MonoBehaviour
{
    void Update()
    {
        // Check for user input to restart the scene
        if (Input.GetKeyDown(KeyCode.BackQuote)) // Backquote (`) key
        {
            RestartScene();
        }
    }

    void RestartScene()
    {
        // Reload the current scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
