using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

[System.Serializable]
public class TemperatureReceiver : MonoBehaviour
{
    public string serverUrl = "https://jepsu.me/unity-temp";
    private float currentTemperature;

    void Start()
    {
        StartCoroutine(GetTemperatureRepeatedly(2f));
    }

    IEnumerator GetTemperatureRepeatedly(float interval)
    {
        while (true)
        {
            yield return StartCoroutine(GetTemperature());
            yield return new WaitForSeconds(interval);
        }
    }

    IEnumerator GetTemperature()
    {
        UnityWebRequest www = UnityWebRequest.Get(serverUrl);

        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Failed to get temperature data: " + www.error);
        }
        else
        {
            string jsonResponse = www.downloadHandler.text;
            TemperatureData temperatureData = JsonUtility.FromJson<TemperatureData>(jsonResponse);

            currentTemperature = temperatureData.temperature;
            Debug.Log("Received temperature: " + currentTemperature);
        }
    }

    void OnGUI()
    {
        // Display the temperature as a GUI box in the right corner
        int margin = 10;
        int boxWidth = 200;
        int boxHeight = 50;

        Rect boxRect = new Rect(Screen.width - boxWidth - margin, margin, boxWidth, boxHeight);
        GUI.Box(boxRect, "Temperature: " + currentTemperature.ToString("F2"));
    }
}

[System.Serializable]
public class TemperatureData
{
    public float temperature;
}
