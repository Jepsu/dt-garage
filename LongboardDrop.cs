using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongboardDrop : MonoBehaviour
{
    private bool isDropped = false;  
    private Animator longboardAnimator;  

    void Start()
    {
        longboardAnimator = GetComponent<Animator>();  
    }

    void OnMouseDown()
    {
        isDropped = !isDropped; 
        longboardAnimator.SetBool("isDropped", isDropped);  
    }
}
