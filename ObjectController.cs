using UnityEngine;

public class ObjectController : MonoBehaviour
{
    private bool isShot = false;
    private Vector3 initialPosition;
    private Rigidbody rb;

    void Start()
    {
        initialPosition = transform.position;
        rb = GetComponent<Rigidbody>();
    }

    // Handle click events from ShootingManager
    public void ShootOrReturnObject()
    {
        if (!isShot)
        {
            // Shoot the object in a random direction
            ShootObject();
        }
        else
        {
            // Return the object back to its initial position
            ReturnToObject();
        }
    }

    // Handle click events directly on the object
    void OnMouseDown()
    {
        // Return the object back to its initial position
        ReturnToObject();
    }

    void ShootObject()
    {
        isShot = true;

        // Apply force in a random direction
        Vector3 randomDirection = Random.onUnitSphere;
        rb.AddForce(randomDirection * 10f, ForceMode.Impulse);
    }

    void ReturnToObject()
    {
        isShot = false;

        // Reset the object's position and velocity
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        transform.position = initialPosition;
    }
}
