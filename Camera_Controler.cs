using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Controler : MonoBehaviour

{
    public GameObject PivotPoint_GO;
    public float FoV = 60.0f;
    public float min_FoV = 20.0f;
    public float max_FoV = 150.0f;
    public float rate_FoV = 1.0f;

    private string selectedObjectName = ""; // Store the selected object's name

    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            RotateCamera(true);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            RotateCamera(false);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (FoV > min_FoV)
            {
                FoV -= rate_FoV;
            }
            Camera.main.fieldOfView = FoV;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            if (FoV < max_FoV)
            {
                FoV += rate_FoV;
            }
            Camera.main.fieldOfView = FoV;
        }
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100f))
            {
                if (hit.collider.name != null)
                {
                    selectedObjectName = hit.transform.gameObject.name;
                }
            }
        }
    }

    void OnGUI()
    {
        if (!string.IsNullOrEmpty(selectedObjectName))
        {
            // Display the selected object's name in a GUI window
            GUI.Window(0, new Rect(10, 10, 400, 50), ShowObjectName, "Object Name");
        }
    }

    void ShowObjectName(int windowID)
    {
        // Display the name of the currently selected object
        GUI.Label(new Rect(10, 20, 400, 20), selectedObjectName);
    }

    public void RotateCamera(bool right)
    {
        float speed = 0.2f;
        Vector3 pivotPoint = PivotPoint_GO.transform.position;
        if (right)
        {
            transform.RotateAround(pivotPoint, Vector3.up, -speed);
        }
        else
        {
            transform.RotateAround(pivotPoint, Vector3.up, speed);
        }
    }
}
