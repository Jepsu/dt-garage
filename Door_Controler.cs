using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door_Controler : MonoBehaviour

{
    private bool isOpen = false;
    private Animator doorAnimator;

    void Start()
    {
        doorAnimator = GetComponent<Animator>();
    }

    void OnMouseDown()
    {
        isOpen = !isOpen;
        doorAnimator.SetBool("IsOpen", isOpen);
    }
}

